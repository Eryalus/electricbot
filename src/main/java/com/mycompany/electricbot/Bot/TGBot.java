/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Bot;

import com.mycompany.electricbot.Bot.Action.Action;
import com.mycompany.electricbot.Bot.Action.DayData;
import com.mycompany.electricbot.Bot.Action.InvalidDate;
import com.mycompany.electricbot.Bot.Action.InvalidMonth;
import com.mycompany.electricbot.Bot.Action.MonthData;
import com.mycompany.electricbot.Data.DataUtils;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 *
 * @author Eryalus
 */
public class TGBot extends TelegramLongPollingBot {

    private static String BOT_TOKEN = "1876589187:AAGMwN77e3lEeFDODYYIVAnI1hh88OvXUOE";

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String message = update.getMessage().getText();
            Action action = null;
            String commandText;
            if (message.length() > 0) {
                commandText = message.split(" ")[0];
            } else {
                commandText = message;
            }
            switch (commandText) {
                case "/hoy":
                case "/h":
                    action = new DayData(new Date());
                    break;
                case "/mañana":
                case "/m":
                    Date dt = new Date();
                    Calendar c = Calendar.getInstance();
                    c.setTime(dt);
                    c.add(Calendar.DATE, 1);
                    dt = c.getTime();
                    action = new DayData(dt);
                    break;
                case "/dia":
                case "/d":
                    String dateS = message.substring(commandText.length()).trim();
                    DateFormat df = new SimpleDateFormat(DataUtils.USER_DATE_FORMAT);
                    df.setLenient(false);
                    try {
                        Date date = df.parse(dateS);
                        action = new DayData(date);
                    } catch (ParseException ex) {
                        action = new InvalidDate();
                    }
                    break;
                case "/mes":
                    String monthS = message.substring(commandText.length()).trim();
                    DateFormat mf = new SimpleDateFormat(DataUtils.USER_MONTH_DATE_FORMAT);
                    // If no month provided -> current month
                    if(monthS.equals("")){
                        monthS = mf.format(new Date());
                    }
                    mf.setLenient(false);
                    try {
                        Date date = mf.parse(monthS);
                        action = new MonthData(date);
                    } catch (ParseException ex) {
                        action = new InvalidMonth();
                    }
                default:
                    break;
            }
            if (action != null) {
                action.process(update, this);
            }
        }
    }

    @Override
    public void onUpdatesReceived(List<Update> updates) {
        super.onUpdatesReceived(updates); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getBotUsername() {
        return "Asd";
    }

    @Override
    public void onRegister() {
        super.onRegister(); //To change body of generated methods, choose Tools | Templates.
    }

}
