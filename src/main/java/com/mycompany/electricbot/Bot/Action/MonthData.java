/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Bot.Action;

import com.mycompany.electricbot.Bot.TGBot;
import com.mycompany.electricbot.Data.DataUtils;
import com.mycompany.electricbot.Data.Entity.ApiResponse;
import com.mycompany.electricbot.Data.Entity.PVPC;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 *
 * @author Adrian
 */
public class MonthData implements Action {

    private Date d = null;
    private final String month;
    private static final long DAY_LENTH = 86400000L; //ms

    public MonthData(Date d) {
        this.d = d;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DataUtils.USER_MONTH_DATE_FORMAT);
        month = simpleDateFormat.format(d);
    }

    @Override
    public void process(Update update, TGBot bot) {
        //not efficient, change
        ArrayList<ApiResponse> daydata = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(d.getTime());
        int totalDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int day = 0; day < totalDays; day++) {
            Date date = new Date(d.getTime() + day * DAY_LENTH);
            ApiResponse dataForDate = DataUtils.getDataForDate(date);
            daydata.add(dataForDate);
        }
        ArrayList<Object[]> prices = new ArrayList<>();
        int dayCount = 0;
        for (ApiResponse response : daydata) {
            dayCount++;
            if (response != null) {
                double min = Double.MAX_VALUE, max = Double.MIN_VALUE, avg = 0, sum = 0;
                for (PVPC pvpc : response.getPVPC()) {
                    double e_kwh = (DataUtils.getValue(pvpc.getPCB()) / 1000);
                    sum += e_kwh;
                    if (e_kwh < min) {
                        min = e_kwh;
                    }
                    if (e_kwh > max) {
                        max = e_kwh;
                    }
                }
                avg = sum / response.getPVPC().size();
                prices.add(new Object[]{(double) dayCount, avg, max, min});
            }
        }

        BufferedImage image = createChart(prices, totalDays);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
            jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            jpegParams.setCompressionQuality(1f);
            ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();
            ImageIO.write(image, "png", os);
            InputStream is = new ByteArrayInputStream(os.toByteArray());
            SendPhoto sendPhoto = new SendPhoto("" + update.getMessage().getChatId(), new InputFile(is, this.month));
            if (this.d != null) {
                sendPhoto.setCaption("Gráfico");
            }
            bot.execute(sendPhoto);
        } catch (IOException ex) {
            Logger.getLogger(DayData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TelegramApiException ex) {
            Logger.getLogger(DayData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param prices (date, avg, max, min)
     * @param dayCount
     * @return
     */
    private BufferedImage createChart(ArrayList<Object[]> prices, int dayCount) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries series1 = new XYSeries("Media");
        XYSeries series2 = new XYSeries("Máximo");
        XYSeries series3 = new XYSeries("Mínimo");
        int counter = 0;
        for (Object[] price : prices) {
            series1.add((double) price[0], (double) price[1]);
            series2.add((double) price[0], (double) price[2]);
            series3.add((double) price[0], (double) price[3]);
            counter++;
        }
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        dataset.addSeries(series3);
        JFreeChart lineChart = ChartFactory.createXYLineChart(this.month, "Día", "€/kWh", dataset);
        customizeChart(lineChart, dayCount);
        return lineChart.createBufferedImage(1280, 720);
    }

    private void customizeChart(JFreeChart chart, int days) {   // here we make some customization
        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

        // sets paint color for each series
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setSeriesPaint(2, Color.GREEN);

        // sets thickness for series (using strokes)
        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
        renderer.setSeriesStroke(2, new BasicStroke(2.0f));
        plot.getDomainAxis().setRange(1, days);
        // sets paint color for plot outlines
        plot.setOutlinePaint(Color.BLUE);
        plot.setOutlineStroke(new BasicStroke(2.0f));

        // sets renderer for lines
        plot.setRenderer(renderer);

        // sets plot background
        plot.setBackgroundPaint(Color.DARK_GRAY);

        // sets paint color for the grid lines
        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.BLACK);

        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.BLACK);

    }
}
