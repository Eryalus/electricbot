/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Bot.Action;

import com.mycompany.electricbot.Bot.TGBot;
import com.mycompany.electricbot.Data.DataUtils;
import com.mycompany.electricbot.Data.Entity.ApiResponse;
import com.mycompany.electricbot.Data.Entity.PVPC;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 *
 * @author Eryalus
 */
public class DayData implements Action {

    private Date d = null;
    private String date = null;
    private ArrayList<Double> orderedPrices = new ArrayList<>();
    private Double remarkMin = Double.MIN_VALUE;
    private Double remarkMax = Double.MAX_VALUE;
    private Double averagePrice = 0d;

    public DayData(Date d) {
        this.d = d;
    }

    private void setupDayParams(ApiResponse dayInfo) {
        ArrayList<Double> prices = new ArrayList<>();
        Double sum = 0d;
        for (PVPC price : dayInfo.getPVPC()) {
            if (this.date == null) {
                this.date = price.getDia();
            }
            double e_kwh = (DataUtils.getValue(price.getPCB()) / 1000);
            sum += e_kwh;
            prices.add(e_kwh);
        }
        Collections.sort(prices);
        this.orderedPrices = prices;
        this.averagePrice = sum / prices.size();
        if (prices.size() == 24) {
            this.remarkMin = prices.get(2);
            this.remarkMax = prices.get(prices.size() - 3);
        }
    }

    @Override
    public void process(Update update, TGBot bot) {
        ApiResponse dataForDate = DataUtils.getDataForDate(d);
        SendMessage send;
        if (dataForDate != null) {
            setupDayParams(dataForDate);
            String text = "";
            ArrayList<Object[]> prices = new ArrayList<>();
            if (this.date != null) {
                text += "<b>" + this.date + " - " + String.format("%.5f", this.averagePrice) + " €/kWh</b>\n---------------------------------\n";
            }
            for (PVPC price : dataForDate.getPVPC()) {
                double e_kwh = (DataUtils.getValue(price.getPCB()) / 1000);
                prices.add(new Object[]{price.getHora(), e_kwh});
                text += price.getHora() + "h: " + String.format("%.5f", e_kwh) + " €/kWh" + (e_kwh <= this.remarkMin ? " 🔌" : (e_kwh >= this.remarkMax ? " 💸" : "")) + "\n";
            }
            send = new SendMessage("" + update.getMessage().getChatId(), text);
            send.setParseMode(ParseMode.HTML);
            BufferedImage image = createChart(prices);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            try {
                JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
                jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                jpegParams.setCompressionQuality(1f);
                ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();
                ImageIO.write(image, "png", os);
                InputStream is = new ByteArrayInputStream(os.toByteArray());
                SendPhoto sendPhoto = new SendPhoto("" + update.getMessage().getChatId(), new InputFile(is, text));
                if (this.date != null) {
                    sendPhoto.setCaption(this.date);
                }
                bot.execute(sendPhoto);
            } catch (IOException ex) {
                Logger.getLogger(DayData.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TelegramApiException ex) {
                Logger.getLogger(DayData.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            send = new SendMessage("" + update.getMessage().getChatId(), "No hay datos.");
        }
        try {
            bot.execute(send);
        } catch (TelegramApiException ex) {
            Logger.getLogger(DayData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private BufferedImage createChart(ArrayList<Object[]> prices) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Object[] price : prices) {
            dataset.addValue((Double) price[1], "Precio", (String) price[0]);
        }
        JFreeChart lineChart = ChartFactory.createLineChart(null, "Hora", "€/kWh", dataset);
        lineChart.removeLegend();
        CategoryPlot plot = (CategoryPlot) lineChart.getPlot();
        CategoryAxis domainAxis = plot.getDomainAxis();
        Font old = domainAxis.getTickLabelFont();
        Font font = new Font(old.getFamily(), old.getStyle(), old.getSize() - 1);
        domainAxis.setTickLabelFont(font);
        plot.setDomainAxis(domainAxis);
        return lineChart.createBufferedImage(1280, 720);
    }

}
