/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Bot.Action;

import com.mycompany.electricbot.Bot.TGBot;
import com.mycompany.electricbot.Data.DataUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 *
 * @author Adrian
 */
public class InvalidMonth implements Action {

    private static final String TEXT = "Formato de mes inválido. Por favor use el formato " + DataUtils.USER_MONTH_DATE_FORMAT + ".";

    @Override
    public void process(Update update, TGBot bot) {

        SendMessage sm = new SendMessage("" + update.getMessage().getChatId(), TEXT);
        try {
            bot.execute(sm);
        } catch (TelegramApiException ex) {
            Logger.getLogger(InvalidMonth.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
