/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Bot.Action;

import com.mycompany.electricbot.Bot.TGBot;
import org.telegram.telegrambots.meta.api.objects.Update;

/**
 *
 * @author Eryalus
 */
public interface Action {

    public void process(Update update, TGBot bot);
}
