/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Data.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Eryalus
 */
public class PVPC{
    @JsonProperty("Dia") 
    public String getDia() { 
		 return this.dia; } 
    public void setDia(String dia) { 
		 this.dia = dia; } 
    String dia;
    @JsonProperty("Hora") 
    public String getHora() { 
		 return this.hora; } 
    public void setHora(String hora) { 
		 this.hora = hora; } 
    String hora;
    @JsonProperty("PCB") 
    public String getPCB() { 
		 return this.pCB; } 
    public void setPCB(String pCB) { 
		 this.pCB = pCB; } 
    String pCB;
    @JsonProperty("CYM") 
    public String getCYM() { 
		 return this.cYM; } 
    public void setCYM(String cYM) { 
		 this.cYM = cYM; } 
    String cYM;
    @JsonProperty("COF2TD") 
    public String getCOF2TD() { 
		 return this.cOF2TD; } 
    public void setCOF2TD(String cOF2TD) { 
		 this.cOF2TD = cOF2TD; } 
    String cOF2TD;
    @JsonProperty("PMHPCB") 
    public String getPMHPCB() { 
		 return this.pMHPCB; } 
    public void setPMHPCB(String pMHPCB) { 
		 this.pMHPCB = pMHPCB; } 
    String pMHPCB;
    @JsonProperty("PMHCYM") 
    public String getPMHCYM() { 
		 return this.pMHCYM; } 
    public void setPMHCYM(String pMHCYM) { 
		 this.pMHCYM = pMHCYM; } 
    String pMHCYM;
    @JsonProperty("SAHPCB") 
    public String getSAHPCB() { 
		 return this.sAHPCB; } 
    public void setSAHPCB(String sAHPCB) { 
		 this.sAHPCB = sAHPCB; } 
    String sAHPCB;
    @JsonProperty("SAHCYM") 
    public String getSAHCYM() { 
		 return this.sAHCYM; } 
    public void setSAHCYM(String sAHCYM) { 
		 this.sAHCYM = sAHCYM; } 
    String sAHCYM;
    @JsonProperty("FOMPCB") 
    public String getFOMPCB() { 
		 return this.fOMPCB; } 
    public void setFOMPCB(String fOMPCB) { 
		 this.fOMPCB = fOMPCB; } 
    String fOMPCB;
    @JsonProperty("FOMCYM") 
    public String getFOMCYM() { 
		 return this.fOMCYM; } 
    public void setFOMCYM(String fOMCYM) { 
		 this.fOMCYM = fOMCYM; } 
    String fOMCYM;
    @JsonProperty("FOSPCB") 
    public String getFOSPCB() { 
		 return this.fOSPCB; } 
    public void setFOSPCB(String fOSPCB) { 
		 this.fOSPCB = fOSPCB; } 
    String fOSPCB;
    @JsonProperty("FOSCYM") 
    public String getFOSCYM() { 
		 return this.fOSCYM; } 
    public void setFOSCYM(String fOSCYM) { 
		 this.fOSCYM = fOSCYM; } 
    String fOSCYM;
    @JsonProperty("INTPCB") 
    public String getINTPCB() { 
		 return this.iNTPCB; } 
    public void setINTPCB(String iNTPCB) { 
		 this.iNTPCB = iNTPCB; } 
    String iNTPCB;
    @JsonProperty("INTCYM") 
    public String getINTCYM() { 
		 return this.iNTCYM; } 
    public void setINTCYM(String iNTCYM) { 
		 this.iNTCYM = iNTCYM; } 
    String iNTCYM;
    @JsonProperty("PCAPPCB") 
    public String getPCAPPCB() { 
		 return this.pCAPPCB; } 
    public void setPCAPPCB(String pCAPPCB) { 
		 this.pCAPPCB = pCAPPCB; } 
    String pCAPPCB;
    @JsonProperty("PCAPCYM") 
    public String getPCAPCYM() { 
		 return this.pCAPCYM; } 
    public void setPCAPCYM(String pCAPCYM) { 
		 this.pCAPCYM = pCAPCYM; } 
    String pCAPCYM;
    @JsonProperty("TEUPCB") 
    public String getTEUPCB() { 
		 return this.tEUPCB; } 
    public void setTEUPCB(String tEUPCB) { 
		 this.tEUPCB = tEUPCB; } 
    String tEUPCB;
    @JsonProperty("TEUCYM") 
    public String getTEUCYM() { 
		 return this.tEUCYM; } 
    public void setTEUCYM(String tEUCYM) { 
		 this.tEUCYM = tEUCYM; } 
    String tEUCYM;
    @JsonProperty("CCVPCB") 
    public String getCCVPCB() { 
		 return this.cCVPCB; } 
    public void setCCVPCB(String cCVPCB) { 
		 this.cCVPCB = cCVPCB; } 
    String cCVPCB;
    @JsonProperty("CCVCYM") 
    public String getCCVCYM() { 
		 return this.cCVCYM; } 
    public void setCCVCYM(String cCVCYM) { 
		 this.cCVCYM = cCVCYM; } 
    String cCVCYM;
    @JsonProperty("EDSRPCB") 
    public String getEDSRPCB() { 
		 return this.eDSRPCB; } 
    public void setEDSRPCB(String eDSRPCB) { 
		 this.eDSRPCB = eDSRPCB; } 
    String eDSRPCB;
    @JsonProperty("EDSRCYM") 
    public String getEDSRCYM() { 
		 return this.eDSRCYM; } 
    public void setEDSRCYM(String eDSRCYM) { 
		 this.eDSRCYM = eDSRCYM; } 
    String eDSRCYM;
}
