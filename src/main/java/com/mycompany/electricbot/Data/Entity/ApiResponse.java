/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Data.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author Eryalus
 */
public class ApiResponse {
    @JsonProperty("PVPC") 
    public List<PVPC> getPVPC() { 
		 return this.pVPC; } 
    public void setPVPC(List<PVPC> pVPC) { 
		 this.pVPC = pVPC; } 
    List<PVPC> pVPC;
}
