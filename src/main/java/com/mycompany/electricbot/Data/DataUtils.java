/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.electricbot.Data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.electricbot.Data.Entity.ApiResponse;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Eryalus
 */
public class DataUtils {

    public static final String USER_DATE_FORMAT = "dd/MM/yyyy";
    public static final String USER_MONTH_DATE_FORMAT = "MM/yyyy";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String DATA_FOLDER = "data";
    private static final Semaphore mapSemaphore = new Semaphore(1);
    private static final HashMap<String, Semaphore> semaphores = new HashMap<>();

    public static double getValue(String add) {
        return Double.parseDouble(add.replace(",", "."));
    }

    public static ApiResponse getDataForDate(Date d) {
        ApiResponse map = null;
        try {
            mapSemaphore.acquire();
            if (d == null) {
                d = new Date();
            }
            String dateFormatted = getDateFormatted(d);
            Semaphore sem = semaphores.get(dateFormatted);
            if (sem == null) {
                sem = new Semaphore(1);
            }
            semaphores.put(dateFormatted, sem);
            mapSemaphore.release();
            sem = semaphores.get(dateFormatted);
            sem.acquire();
            try {
                String file = getFileForDay(d);
                File fileAsFile = new File(file);
                ObjectMapper om = new ObjectMapper();
                if (fileAsFile.exists()) {
                    map = om.readValue(fileAsFile, ApiResponse.class);
                } else {
                    URL url = new URL("https://api.esios.ree.es/archives/70/download_json" + dateToParam(d));
                    HttpURLConnection http = (HttpURLConnection) url.openConnection();
                    map = om.readValue(http.getInputStream(), ApiResponse.class);
                    http.disconnect();
                    new File(DATA_FOLDER).mkdirs();
                    om.writeValue(fileAsFile, map);
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            sem.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(DataUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    private static String getFileForDay(Date d) {
        if (d == null) {
            d = new Date();
        }
        return DATA_FOLDER + "/" + getDateFormatted(d) + ".json";
    }

    private static String getDateFormatted(Date d) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat.format(d);
    }

    public static String dateToParam(Date d) {
        String s = "";
        if (d != null) {
            s = "?date=" + getDateFormatted(d);
        }
        return s;
    }
}
